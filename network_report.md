## Assignment #1: FTP Server and Client
## 许文 5120369039


### 1. Brief
----
I have implemented my ftp server and client with Python, since as a script language, Python has strong support on network programming and plenty of related interfaces(socket library).

The ftp server supports multi-users and is implemented based on RFC 959, which descibes the FILE TRANSFER PROTOCOL (FTP). I have a minimum implementation of the protocol, including login, listing files, uploading files, downloading files and etc.

The client has a user-friendly commandline-style UI. The users do not need to type in the RAW commands of FTP protocol to interact with the ftp server. All the commands which the users use are easy understanding.

### 2. FTP Server Part
----
The ftp server interacts with the client by two connections. One is the control connections and the other is the data connections. 

At first I create a socket for control use and it waits for the client's connection, the default bind port is 1337. I implemented a Server class, which is a multi-threading class. Once a client connects to that port of the server, a new instance of this Server class will begin to run.

The server then tries to receive the request from the client in a dead loop. Once it gets a request, it tries to make out which commands the request contains. Currently the raw commands supported in my ftp server are USER, QUIT, HELP, TYPE, RETR, STOR, CWD, PWD, PASS, PASV, NLST, LIST. 

Currently the ftp server only supports the passive data transfer mode, which means that if there is some data transfer between the server and the client, the server will create a new socket called data socket, and randomly choose a another bind port, which is different from the bind port of the control socket. The server will give the bind address and data port to the client. Then the server will create a new instance of class DataServer, which is also a multi-threading class and be waiting for the clients to connect to this data port.

Actually all the commands and the status(feedback) of the execution of the commands are transferred through the control socket channel. They are transferred all in ascii mode. All the feedback information including the reply code is according to RFC 959.

If some data is going to be transferred through the data socket, then the server will make sure that the passive mode is already on and the data server is accepting now.

Here is some explanation of these raw commands.

##### 1. USER
This command is used to provide the username of the current user. 

##### 2. PASS
This command is used to provide the password of the current user. My server has a default login account:
	
	username : test
	password : test

##### 3. QUIT
This command is used to leave the ftp server. The client's connection will be closed then.

##### 4. TYPE
This command determines two types of the data when transferred. This one supports `TYPE A`(ascii mode) and `TYPE I`(binary mode).

##### 5. HELP
This command lets the server to provide the valid raw commands of the ftp server.

##### 6. PASV
This command make the server turn into the passive mode. A new socket called data socket will be created. Then it will bind to a port and return the information of the bind address and bind port to the client in a 5-tuple.

##### 7. NLST
For this one, I just use Python API `os.listdir(current directory)` to get all the filenames under the current remote directory and return them back to the client through the data socket in ascii mode.

##### 8. LIST
For this one I just try to execute shell command `ls -al (current directory)` to get all the information of all the files under the current remote directory and return it back to the client through the data socket in ascii mode.

##### 9. PWD
The server tells the location of the current remote directory to client and this is done through the control socket in ascii mode!

##### 10. CWD
This command should have an argument which represents the target directory the user wants to go to. The server will change the current remote directory to it if possible.

##### 11. RETR
This command is used by the user to get files from the ftp server. The argument represents the location of the target file. The file content is transferred through the data socket in either binary mode(most commonly) or ascii mode.

##### 12. STOR
This command is used by the user to upload files to the ftp server. The argument represents the location of the file you want to upload in the local file system. The file content is transferred through the data socket in either binary mode(most commonly) or ascii mode.

### 3. Client Part
----
The implementation of the client is simpler than that of the server. For the client, every time the user needs to type in the client commands after a 'ftp> ' prompt. The client gets your commands and make it into the corresponding raw ftp commands and send them to the remote ftp server.

When some data transfer is about to start and the data server has not run yet, the client should give a `pasv` request to the server and get the server' reply which includes the data socket address and port, and connect to it. All the data transfer is through the data socket connection between the server and the client.

Currently it supports the following commands:

##### 1. connect
Specify the ip address and port of remote ftp server you want to visit. And the client will try to connect to it.

##### 2. login

	USER username
	PASS passwd
	
Input your username and then password to login your ftp server account.

##### 3. help
List the valid client commands you can use.

##### 4. rhelp
	HELP

List the valid raw ftp commands supported by the remote ftp server.

##### 5. quit
	QUIT

Disconnect from the remote ftp server.

##### 6. pwd
	PWD
	
Get the current remote directory.

##### 7. cd arg
	CWD arg

Change the current remote directory to the location where the argument indicates.

##### 8. pasv
	PASV

Turn into the passive mode. Get the remote data server socket's IP address and port.

##### 9. ls
	TYPE A
	PASV if needed
	LIST

Change into the ascii mode and get the information of all the files under the current remote directory through the data connection.

##### 10. nlist
	TYPE A
	PASV if needed
	NLST
	
Change into the ascii mode and get the name lists of all the files under the current remote directory through the data connection.

##### 11. type [ascii|binary]
	TYPE A / 
	TYPE I

Change into ascii data transfer mode or ascii transfer mode.

##### 12. get filename
	PASV if needed
	RETR filename

Download the file on the remote server. The content of the file is transferred through the data connection in binary mode as common.

##### 12. get filename
	PASV if needed
	STOR filename

Upload the file onto the remote server. The content of the file is transferred through the data connection in binary mode as common.









