#!/usr/bin/env python
# encoding: utf-8

import threading, os, time, sys, socket, commands

reply_code = {
    125 : b'125 Data connection already open; transfer starting.\r\n',
    220 : b'220 Service ready for new user.\r\n',
    221 : b'221 Goodbye.\r\n',
    501 : b'501 Syntax error in parameters or arguments.\r\n',
    331 : b'331 User name okay, need password.\r\n',
    503 : b'503 Bad sequence of commands.\r\n',
    214 : b'214 USER PASS HELP QUIT RETR STOR TYPE PORT PASV PWD CWD NLST LIST\r\n',
    530 : b'530 Not logged in.\r\n',
    257 : b'257 Remote directory: %s\r\n',
    200 : b'200 Type set to %s\r\n',
    504 : b'504 Command not implemented for that parameter.\r\n',
    250 : b'250 CWD command successful\r\n',
    550 : b'550 Requested action not taken. File unavailable (e.g., file not found, no access).\r\n',
    227 : b'227 Entering passive mode (%s)\r\n',
    226 : b'226 Closing data connection. Transfer complete.\r\n',
    425 : b'425 Can\'t open data connection.\r\n',
    230 : b'230 User logged in, proceed.\r\n',
    530 : b'530 Not logged in.\r\n',
}

user = {
    'test' : 'test',
}

ftp_addr = '127.0.0.1'
ftp_port = 1337

def current_time(): return time.strftime('%Y-%m-%d %H:%M:%S')

def log(msg, client=None):
    if client != None:
        print '%s %s (%s:%d)' % (current_time(), msg, client[0], client[1])
    else:
        print '%s %s' % (current_time(), msg)
    return

class DataServer(threading.Thread):
    def __init__(self, server):
        threading.Thread.__init__(self)
        self.daemon = True
        self.server = server
        self.server_socket = server.data_server_socket

    def run(self):
        self.server_socket.settimeout(1.0)
        while True:
            try:
                (client_socket, client_addr) = self.server_socket.accept()
            except (socket.timeout):
                pass
            except (socket.error):
                break
            else:
                if self.server.data_socket != None:
                    client_socket.close()
                    log('Connection refused from %s:%d. ' % (client_addr[0], client_addr[1]), self.server.client_addr)
                else:
                    self.server.data_socket = client_socket
                    log('Connection built from %s:%d.' % (client_addr[0], client_addr[1]), self.server.client_addr)

class Server(threading.Thread):
    def __init__(self, control_socket, client_addr):
        threading.Thread.__init__(self)
        self.daemon = True

        self.buf_size = 1024

        self.control_socket = control_socket
        self.client_addr = client_addr

        self.data_socket = None
        self.data_server_socket = None
        self.data_server_addr = '127.0.0.1'
        self.data_server_port = None

        self.username = None
        self.login = False

        self.cwd = os.getcwd()

        self.type_mode = 'I' # ASCII
        self.data_mode = 'PORT'

    def reply(self, code):
        self.control_socket.send(reply_code[code].encode('ascii'))

    def reply_msg(self, code, content):
        self.control_socket.send((reply_code[code] % content).encode('ascii'))

    def reply_data(self, content):
        if self.type_mode == 'A':
            self.data_socket.send(content.encode('ascii'))
        else:
            self.data_socket.send(content)

    def recv_request(self):
        return self.control_socket.recv(self.buf_size)

    def connection_close(self):
        self.control_socket.close()
        log('Client disconnected.', self.client_addr)

    def run(self):
        self.reply(220)

        while True:
            client_req = self.recv_request()

            if len(client_req) == 0:
                self.reply(221)
                self.connection_close()
                break
            client_cmd = client_req.split()[0].upper()

            if client_cmd == 'USER':
                if len(client_req.split()) < 2:
                    self.reply(501)
                else:
                    self.username = client_req.split()[1]
                    self.reply(331)
                    self.login = False

            elif client_cmd == 'QUIT':
                self.reply(221)
                self.connection_close()
                break
            elif client_cmd == 'PORT':
                pass
            elif client_cmd == 'TYPE':
                if not self.login:
                    self.reply(530)
                else:
                    if len(client_req.split()) < 2:
                        self.reply(501)
                    else:
                        c = client_req.split()[1]
                        if c == 'A':
                            self.type_mode = 'A'
                            self.reply_msg(200, 'A')
                        elif c == 'I':
                            self.type_mode = 'I'
                            self.reply_msg(200, 'I')
                        else:
                            self.reply(504)

            #elif client_cmd == 'MODE':
            elif client_cmd == 'RETR':
                if not self.login:
                    self.reply(530)
                else:
                    if len(client_req.split()) < 2:
                        self.reply(501)
                    elif self.data_mode == 'PASV' and self.data_socket != None:
                        binary_dir = os.getcwd()
                        os.chdir(self.cwd)
                        self.reply(125)
                        filename = client_req.split()[1]
                        try:
                            content = open(filename, 'rb').read()
                            time.sleep(0.5)
                            self.reply_data(content)
                        except:
                            self.reply(550)
                        self.data_socket.close()
                        self.data_socket = None
                        self.reply(226)
                        os.chdir(binary_dir)
                    else:
                        self.reply(425)

            elif client_cmd == 'STOR':
                if not self.login:
                    self.reply(530)
                else:
                    if len(client_req.split()) < 2:
                        self.reply(501)
                    elif self.data_mode == 'PASV' and self.data_socket != None:
                        binary_dir = os.getcwd()
                        os.chdir(self.cwd)
                        self.reply(125)
                        filename = client_req.split()[1]
                        try:
                            fd = open(filename, 'wb')
                        except:
                            self.reply(550)

                        time.sleep(0.5)
                        #self.data_socket.setblocking(False)
                        while True:
                            try:
                                data = self.data_socket.recv(self.buf_size)
                                if data == b'': break
                                fd.write(data)
                            except (socket.error): break
                        fd.close()
                        self.data_socket.close()
                        self.data_socket = None
                        self.reply(226)
                        os.chdir(binary_dir)
                    else:
                        self.reply(425)

            elif client_cmd == 'HELP':
                self.reply(214)

            elif client_cmd == 'PASS':
                if self.username == None:
                    self.reply(503)
                    continue

                if len(client_req.split()) < 2:
                    self.reply(501)
                else:
                    if user.has_key(self.username) and user[self.username] == client_req.split()[1]:
                        self.reply(230)
                        self.login = True
                    else:
                        self.reply(530)

            elif client_cmd == 'PWD':
                if not self.login:
                    self.reply(530)
                else:
                    self.reply_msg(257, self.cwd)

            elif client_cmd == 'CWD':
                if not self.login:
                    self.reply(530)
                else:
                    if len(client_req.split()) < 2:
                        self.reply(250)
                    else:
                        binary_dir = os.getcwd()
                        os.chdir(self.cwd)
                        target_dir = client_req.split()[1]
                        try:
                            os.chdir(target_dir)
                        except (OSError):
                            self.reply(550)
                        else:
                            self.cwd = os.getcwd()
                            self.reply(250)
                        os.chdir(binary_dir)

            elif client_cmd == 'PASV':
                if not self.login:
                    self.reply(530)
                else:
                    if self.data_server_socket != None: self.data_server_socket.close()
                    self.data_server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
                    self.data_server_socket.bind((self.data_server_addr, 0))
                    self.data_server_port = self.data_server_socket.getsockname()[1]
                    self.data_server_socket.listen(5)
                    self.data_mode = 'PASV'
                    DataServer(self).start()
                    time.sleep(0.5)
                    self.reply_msg(227, '%s,%s,%s,%s,%d,%d' %
                            (self.data_server_addr.split('.')[0], self.data_server_addr.split('.')[1],
                            self.data_server_addr.split('.')[2], self.data_server_addr.split('.')[3],
                            int(self.data_server_port / 256), self.data_server_port % 256))

            elif client_cmd == 'NLST':
                if not self.login:
                    self.reply(530)
                else:
                    if self.data_mode == 'PASV' and self.data_socket != None:
                        self.reply(125)
                        content = '\r\n'.join(os.listdir(self.cwd)) + '\r\n'
                        orig_type_mode = self.type_mode
                        self.type_mode = 'A'
                        self.reply_data(content)
                        self.type_mode = orig_type_mode
                        self.data_socket.close()
                        self.data_socket = None
                        self.reply(226)
                    else:
                        self.reply(425)

            elif client_cmd == 'LIST':
                #print self.data_socket
                if not self.login:
                    self.reply(530)
                else:
                    if self.data_mode == 'PASV' and self.data_socket != None:
                        self.reply(125)
                        a, b = commands.getstatusoutput('ls -al %s' % (self.cwd))
                        b = b.replace('\n', '\r\n') + '\r\n'
                        orig_type_mode = self.type_mode
                        self.type_mode = 'A'
                        self.reply_data(b)
                        self.type_mode = orig_type_mode
                        self.data_socket.close()
                        self.data_socket = None
                        self.reply(226)
                    else:
                        self.reply(425)
        return

if __name__ == '__main__':
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind((ftp_addr, ftp_port))
    s.listen(5)

    log('The FTP server started.')

    while True:
        (control_socket, client_addr) = s.accept()
        Server(control_socket, client_addr).start()
        log('Client connected.', client_addr)


