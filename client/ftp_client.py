#!/usr/bin/env python
# encoding: utf-8

import socket, sys, os, re, time

default_port = 1337

control_socket = None
buf_size = 1024
connected = False
login = False
data_mode = 'PORT'
data_client_addr = None
type_mode = 'I'

data_server_addr = None
data_server_port = None

def recv_reply():
    global control_socket, connected, login
    if control_socket == None: return 0

    try:
        reply = control_socket.recv(buf_size)
        reply = reply.decode('ascii').strip()
    except (socket.timeout):
        return -1
    else:
        if len(reply) > 0:
            status = int(reply.split(' ')[0][0]) # first digit <= 3 means OK
            #print reply
            return status, reply
        else:
            control_socket.close()
            control_socket = None
            connected = False
            login = False
            return -1

def send_cmd(content):
    global control_socket
    control_socket.send(content.encode('ascii'))

def recv_data(s):
    global buf_size
    data = ''
    while True:
        try:
            n = s.recv(buf_size)
            if len(n) > 0: data += n
            else: break
        except (socket.error):
            break

    if type_mode == 'A': return data.decode('ascii')
    else: return data

def connect(host, port):
    global control_socket, login, connected
    if control_socket != None:
        connected = False
        login = False
        control_socket.close()
        control_socket = None
        return -1
    else:
        control_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
        control_socket.connect((host, port))
        status, reply = recv_reply()
        print reply
        if status <= 3:
            connected = True
            control_socket.settimeout(1.0)
            return 0
        else:
            return -1

def log_in(user):
    global login
    if not connected: return -1
    login = False
    send_cmd('USER %s\r\n' % user)
    status, reply = recv_reply()
    print reply
    if status <= 3:
        passwd = raw_input('(password for %s): ' % user)
        send_cmd('PASS %s\r\n' % passwd.strip())
        status, reply = recv_reply()
        print reply
        if status <= 3:
            login = True
            return 0
    return -1

def quit():
    global control_socket, login, connected
    if not connected: return -1
    send_cmd('QUIT\r\n')
    status, reply = recv_reply()
    print reply
    connected = False
    login = False
    control_socket.close()
    control_socket = None
    return 0

def pwd():
    if not connected or not login: return -1
    send_cmd('PWD\r\n')
    status, reply = recv_reply()
    print reply
    return (status <= 3) and 0 or -1

def cwd(path):
    if not connected or not login: return -1
    send_cmd('CWD %s\r\n' % path)
    status, reply = recv_reply()
    print reply
    return (status <= 3) and 0 or -1

def help():
    print 'Valid commands:'
    print 'connect\tlogin\thelp\tquit'
    print 'nlist\tls\tpwd\tcd'
    print 'type\tpasv\tget\tput'

def type(t):
    global type_mode
    if not connected or not login: return -1
    send_cmd('TYPE %s\r\n' % t)
    status, reply = recv_reply()
    print reply
    if status <= 3:
        type_mode = t
        return 0
    return -1

def pasv():
    global data_server_addr, data_server_port, data_mode
    #if not connected or not login: return -1
    send_cmd('PASV\r\n')
    time.sleep(0.1)
    status, reply = recv_reply()
    print reply
    if status <= 3:
        pattern = re.search(r'(\d+),(\d+),(\d+),(\d+),(\d+),(\d+)', reply)
        data_server_addr = pattern.group(1) + '.' + pattern.group(2) + '.' + pattern.group(3) + '.' + pattern.group(4)
        data_server_port = int(pattern.group(5)) * 256 + int(pattern.group(6))
        data_mode = 'PASV'
        return 0
    return -1

def nlst():
    global data_server_addr, data_server_port
    if not connected or not login: return -1
    if data_server_addr == None: pasv()

    if type_mode != 'A': type('A')

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
    s.connect((data_server_addr, data_server_port))

    time.sleep(0.1)
    send_cmd('NLST\r\n')

    #s.setblocking(False)
    data = recv_data(s)
    s.close()

    status, reply = recv_reply()
    print reply.split('\r\n')[0]
    print data
    print reply.split('\r\n')[1]
    return status <= 3 and 0 or -1

def list():
    global data_server_addr, data_server_port
    if not connected or not login: return -1
    if data_server_addr == None: pasv()

    if type_mode != 'A': type('A')

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
    s.connect((data_server_addr, data_server_port))

    time.sleep(0.1)
    send_cmd('LIST\r\n')

    #s.setblocking(False)
    data = recv_data(s)
    s.close()

    status, reply = recv_reply()
    print reply.split('\r\n')[0]
    print data
    print reply.split('\r\n')[1]
    return status <= 3 and 0 or -1

def retr(filename):
    global data_server_addr, data_server_port
    if not connected or not login: return -1
    if data_server_addr == None: pasv()

    if type_mode != 'I': type('I')

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
    s.connect((data_server_addr, data_server_port))

    time.sleep(0.1)
    send_cmd('RETR %s\r\n' % filename)
    f = open(filename, 'wb')
    #s.setblocking(False)

    data = recv_data(s)
    f.write(data)

    f.close()
    s.close()

    status, reply = recv_reply()
    print reply
    #rstatus, reply = recv_reply()
    #print reply
    return status <= 3 and 0 or -1

def send_data(s, content):
    if type_mode == 'A': content = content.encode('ascii')
    s.send(content)

def stor(filename):
    global data_server_addr, data_server_port
    if not connected or not login: return -1
    if data_server_addr == None: pasv()

    if type_mode != 'I': type('I')

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
    s.connect((data_server_addr, data_server_port))

    time.sleep(0.1)
    send_cmd('STOR %s\r\n' % filename)

    try:
       content = open(filename, 'rb').read()
    except:
        return -1

    send_data(s, content)
    s.close()

    status, reply = recv_reply()
    print reply
    status, reply = recv_reply()
    print reply
    return status <= 3 and 0 or -1

def rhelp():
    send_cmd('HELP\r\n')
    status, reply = recv_reply()
    print reply
    return status <= 3 and 0 or -1

def wrong():
    print '?Invalid commands'

if __name__ == '__main__':
    print 'Welcome to FTP client by Wen Xu'
    print 'Network 2015 Big Project'

    while True:
        user_input = raw_input('ftp> ')
        user_input = user_input.strip()
        if len(user_input) == 0: continue
        try:
            cmd = user_input.split()[0]
        except:
            wrong()
            continue

        if cmd == 'connect':
            if len(user_input.split()) < 2:
                wrong()
            else:
                args = user_input.split()[1]
                addr = args.split(':')
                if len(addr) == 2:
                    addr = addr[0]
                    port = int(addr[1])
                else:
                    addr = addr[0]
                    port = default_port
                connect(addr, port)
        elif cmd == 'login':
            username = raw_input('(username): ')
            #passwd = raw_input('(password for %s)' % username)
            log_in(username.strip())
        elif cmd == 'help':
            help()
        elif cmd == 'rhelp':
            rhelp()
        elif cmd == 'quit':
            quit()
            sys.exit(0)
        elif cmd == 'pwd':
            pwd()
        elif cmd == 'cd':
            if len(user_input.split()) < 2:
                wrong()
            else:
                args = user_input.split()[1]
                cwd(args)
        elif cmd == 'ls':
            list()
        elif cmd == 'nlist':
            nlst()
        elif cmd == 'pasv':
            pasv()
        elif cmd == 'type':
            if len(user_input.split()) < 2:
                wrong()
            else:
                args = user_input.split()[1]
                if args == 'binary': type('I')
                elif args == 'ascii': type('A')
                else: wrong()
        elif cmd == 'get':
            if len(user_input.split()) < 2:
                wrong()
            else:
                args = user_input.split()[1]
                retr(args)
        elif cmd == 'put':
            if len(user_input.split()) < 2:
                wrong()
            else:
                args = user_input.split()[1]
                stor(args)
        else: wrong()

